/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/profile.js":
/*!*************************************!*\
  !*** ./public/assets/js/profile.js ***!
  \*************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initSections();\r\n    initUpdateForm();\r\n    initChangeForm();\r\n  });\r\n\r\n  //Functions\r\n  initSections = function(){\r\n    $(\"button\").on(\"click\", function(){\r\n      var section = $(this).attr(\"data-section\");\r\n\r\n      $(this).parent().siblings(\".active\").removeClass(\"active\");\r\n      $(this).parent().addClass(\"active\");\r\n\r\n      $(\".section.active\").removeClass(\"active\");\r\n      $(\"#\"+section).addClass(\"active\");\r\n    });\r\n  };\r\n\r\n  initUpdateForm = function(){\r\n    $(\"#update-form\").validate({\r\n      rules: {\r\n        name: {\r\n          required: true,\r\n          minlength: 3,\r\n          maxlength: 60\r\n        },\r\n        address: {\r\n          maxlength: 60\r\n        },\r\n        city: {\r\n          maxlength: 60\r\n        },\r\n        telephone: {\r\n          required: true,\r\n          maxlength: 15,\r\n          phone: true\r\n        },\r\n        email: {\r\n          required: true,\r\n          email: true,\r\n          maxlength: 60\r\n        }\r\n      }\r\n    });\r\n  };\r\n\r\n  initChangeForm = function(){\r\n    $(\"#change-form\").validate({\r\n      rules: {\r\n        password: {\r\n          required: true,\r\n          minlength: 8,\r\n          maxlength: 255\r\n        },\r\n        password_conf: {\r\n          required: true,\r\n          equalTo: '#password'\r\n        }\r\n      }\r\n    });\r\n  }\r\n\r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/profile.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/profile.js"]();
/******/ 	
/******/ })()
;