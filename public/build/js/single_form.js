/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/single_form.js":
/*!*****************************************!*\
  !*** ./public/assets/js/single_form.js ***!
  \*****************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initBackButton();\r\n  });\r\n\r\n  //Functions\r\n  initBackButton = function(){\r\n    $(\".back-btn\").on(\"click\", function(){\r\n      window.history.back();\r\n    });\r\n  };\r\n  \r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/single_form.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/single_form.js"]();
/******/ 	
/******/ })()
;