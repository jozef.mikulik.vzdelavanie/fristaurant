/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/menu.js":
/*!**********************************!*\
  !*** ./public/assets/js/menu.js ***!
  \**********************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initAddToCart();\r\n  });\r\n\r\n  //Functions\r\n  initAddToCart = function(){\r\n    $(\".price.button\").on(\"click\", function(){\r\n      $.ajax({\r\n        url: \"/menu\",\r\n        type: \"post\",\r\n        data: {\r\n          id: $(this).parent().attr(\"data-id\")\r\n        },\r\n        success:function(response) {\r\n          if(response.message) {\r\n            $(response.message).appendTo(\"body\").fadeIn(500).delay(1500).fadeOut(500, function(){ $(this).remove() });\r\n          } else {\r\n            $(\".cart-wrp .item-count\").text(response.count);\r\n          }\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/menu.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/menu.js"]();
/******/ 	
/******/ })()
;