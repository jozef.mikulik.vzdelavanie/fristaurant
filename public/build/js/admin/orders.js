/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/admin/orders.js":
/*!******************************************!*\
  !*** ./public/assets/js/admin/orders.js ***!
  \******************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initCheckOrders();\r\n\r\n    initAcceptOrder();\r\n    initRejectOrder();\r\n    initDeliveredOrder();\r\n\r\n    initNumberSelector();\r\n  });\r\n\r\n  //Functions\r\n  initCheckOrders = function(){\r\n    checkNewOrders = function(){\r\n      $.ajax({\r\n        url: \"/orders\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"check\",\r\n        },\r\n        success:function(response) {\r\n          if(response.more) {\r\n            $(response.more).appendTo(\".new\").hide().slideDown(500);\r\n          }\r\n          \r\n        }\r\n      });\r\n    }\r\n    setInterval(checkNewOrders, 10000);\r\n  }\r\n\r\n  initRejectOrder = function(){\r\n    $(\".new\").on(\"click\", \".order .admin .btn-reject\", function(){\r\n      $order = $(this).parents(\".order\");\r\n\r\n      $.ajax({\r\n        url: \"/orders\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"reject\",\r\n          id: $order.attr(\"data-id\")\r\n        },\r\n        success:function() {\r\n          $order.slideUp(500, function(){ $(this).remove });\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initAcceptOrder = function(){\r\n    $(\".new\").on(\"click\", \".order .admin .btn-accept\", function(){\r\n      var $admin = $(this).parent();\r\n      var $order = $(this).parents(\".order\");\r\n      var time = $(this).siblings(\".number-selector\").children(\".number\").text();\r\n      \r\n      $.ajax({\r\n        url: \"/orders\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"accept\",\r\n          id: $order.attr(\"data-id\"),\r\n          time: time\r\n        },\r\n        success:function(response) {\r\n          $admin.delay(250).html('<p class=\"time\">' + time + 'm</p><button class=\"btn-delivered\">Delivered</button>');\r\n          $order.slideUp(500, function(){ $order.remove() });\r\n          $order.clone().appendTo(\".accepted\").hide().slideDown(500);\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initDeliveredOrder = function(){\r\n    $(\".accepted\").on(\"click\", \".order .admin .btn-delivered\", function(){\r\n      var $order = $(this).parents(\".order\");\r\n      \r\n      $.ajax({\r\n        url: \"/orders\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"delivered\",\r\n          id: $order.attr(\"data-id\")\r\n        },\r\n        success:function() {\r\n          $order.slideUp(500, function(){ $order.remove() });\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initNumberSelector = function(){\r\n    var $numberSelector = $(\".number-selector\");\r\n    var step            = parseInt($numberSelector.attr(\"data-step\"));\r\n    var min             = parseInt($numberSelector.attr(\"data-min\"));\r\n    var max             = parseInt($numberSelector.attr(\"data-max\"));\r\n\r\n    $(\".new\").on(\"click\", \".order .admin .number-selector .plus\", function(){\r\n      var val = parseInt($(this).siblings(\".number\").text());\r\n      if(val + step <= max)\r\n        $(this).siblings(\".number\").text(val + step);\r\n    });\r\n\r\n    $(\".new\").on(\"click\", \".order .admin .number-selector .minus\", function(){\r\n      var val = parseInt($(this).siblings(\".number\").text());\r\n      if(val - step >= min)\r\n        $(this).siblings(\".number\").text(val - step);\r\n    });\r\n  }\r\n  \r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/admin/orders.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/admin/orders.js"]();
/******/ 	
/******/ })()
;