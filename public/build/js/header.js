/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/header.js":
/*!************************************!*\
  !*** ./public/assets/js/header.js ***!
  \************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initDropdownNav();\r\n    initSignedInDropdown();\r\n  });\r\n\r\n  //Functions\r\n  initSignedInDropdown = function(){\r\n    $(\".signedin\").on({\r\n      mouseenter: function(){\r\n        $(this).children(\".drp-menu-wrp\").stop().slideDown(250);\r\n      },\r\n      mouseleave: function(){\r\n        $(this).children(\".drp-menu-wrp\").stop().slideUp(250);\r\n      }\r\n    });\r\n  };\r\n\r\n  initDropdownNav = function(){\r\n    var $icon = $(\"header .navigation-drp-icon\");\r\n    var $iTop = $(\"header .navigation-drp-icon .top\");\r\n    var $iMid = $(\"header .navigation-drp-icon .mid\");\r\n    var $iBot = $(\"header .navigation-drp-icon .bot\");\r\n    var $dropdown = $(\"header .navigation-drp\");\r\n\r\n    var navHidden = true;\r\n    var duration = 200;\r\n    var top = $iTop.css(\"top\");\r\n    var mid = $iMid.css(\"top\");\r\n    var bot = $iBot.css(\"top\");\r\n\r\n    var changeToX = function(){\r\n      navHidden = false;\r\n      \r\n      $iTop.animate({top: mid}, duration).animate({rotate: \"45deg\"}, duration);\r\n      $iMid.delay(duration).animate({opacity: 0}, 0);\r\n      $iBot.animate({top: mid}, duration).animate({rotate: \"-45deg\"}, duration);\r\n\r\n      $dropdown.slideDown(duration * 2);\r\n    };\r\n\r\n    var changeToBurger = function() {\r\n      navHidden = true;\r\n\r\n      $iTop.animate({rotate: \"0deg\"}, duration).animate({top: top}, duration);\r\n      $iMid.delay(duration).animate({opacity: 1}, 0);\r\n      $iBot.animate({rotate: \"0deg\"}, duration).animate({top: bot}, duration);\r\n      \r\n      $dropdown.slideUp(duration * 2);\r\n    }\r\n\r\n    $icon.on(\"click\", function(){\r\n      navHidden ? changeToX() : changeToBurger();\r\n    });\r\n    \r\n    $(window).on(\"resize\", function(){\r\n      if(!window.matchMedia('(max-width: 1023px)').matches)\r\n        !navHidden ? changeToBurger() : \"\";\r\n    });\r\n    \r\n  }\r\n\r\n  \r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/header.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/header.js"]();
/******/ 	
/******/ })()
;