/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/validation_setup.js":
/*!**********************************************!*\
  !*** ./public/assets/js/validation_setup.js ***!
  \**********************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initValidation();\r\n  });\r\n\r\n  //Functions\r\n  initValidation = function(){\r\n    var emailVal = $(\"#email\").val();\r\n\r\n    jQuery.validator.addMethod(\"notEqualTo\", function(value, element) {\r\n      return value != emailVal;\r\n    });\r\n\r\n    jQuery.validator.addMethod(\"phone\", function(value, element) {\r\n      var pattern = new RegExp('^[+][0-9]{6,14}$');\r\n      return pattern.test(value);\r\n    });\r\n\r\n    jQuery.extend(jQuery.validator.messages, {\r\n      required: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> This field is required',\r\n      email: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter valid e-mail',\r\n      minlength: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter at least {0} characters',\r\n      maxlength: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter no more than {0} characters',\r\n      equalTo: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Passwords do not match',\r\n      notEqualTo: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> This e-mail is already used',\r\n      phone: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter valid telephone number without spaces',\r\n      number: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter a number',\r\n      min: '<img src=\"assets/icons/exclamation.svg\" alt=\"\"> Enter value greather than {0}'\r\n    });\r\n  };\r\n\r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/validation_setup.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/validation_setup.js"]();
/******/ 	
/******/ })()
;