/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/registration.js":
/*!******************************************!*\
  !*** ./public/assets/js/registration.js ***!
  \******************************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initFormValidation();\r\n  });\r\n\r\n  //Functions\r\n  initFormValidation = function(){\r\n    $(\"form\").validate({\r\n      rules: {\r\n        name: {\r\n          required: true,\r\n          minlength: 3,\r\n          maxlength: 60\r\n        },\r\n        address: {\r\n          maxlength: 60\r\n        },\r\n        city: {\r\n          maxlength: 60\r\n        },\r\n        telephone: {\r\n          required: true,\r\n          maxlength: 15,\r\n          phone: true\r\n        },\r\n        email: {\r\n          required: true,\r\n          email: true,\r\n          maxlength: 60,\r\n          notEqualTo: true\r\n        },\r\n        password: {\r\n          required: true,\r\n          minlength: 8,\r\n          maxlength: 255\r\n        },\r\n        password_conf: {\r\n          required: true,\r\n          equalTo: '#password'\r\n        }\r\n      }\r\n    });\r\n  }\r\n\r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/registration.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/registration.js"]();
/******/ 	
/******/ })()
;