/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./public/assets/js/cart.js":
/*!**********************************!*\
  !*** ./public/assets/js/cart.js ***!
  \**********************************/
/***/ (() => {

eval("(function ($) {\r\n\r\n  //Initialize functions\r\n  $(document).ready(function () {\r\n    initRemoveMeal();\r\n    initEmptyCart();\r\n    initOrder();\r\n\r\n    initNumberSelector();\r\n  });\r\n\r\n  //Other functions\r\n  emptyCart = function(){\r\n    $(\".filled\").fadeOut(500, function(){ \r\n      $(\".empty\").fadeIn(500);\r\n      $(this).remove();\r\n    });\r\n  }\r\n\r\n  //Functions\r\n  initRemoveMeal = function(){\r\n    $(\".meal .delete\").on(\"click\", function(){\r\n      var $meal = $(this).parent();\r\n\r\n      $.ajax({\r\n        url: \"/cart\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"removeMeal\",\r\n          id: $meal.attr(\"id\")\r\n        },\r\n        success:function(response) {\r\n          var $meals = $(\".meals .meal\").length - 1;\r\n          if($meals == 0) {\r\n            emptyCart();\r\n          } else {\r\n            var price = parseFloat($meal.find(\".price strong\").text());\r\n            var globalPrice = parseFloat($(\".summ .price strong\").text());\r\n            $(\".summ .price strong\").text((globalPrice - price).toFixed(2));\r\n            $meal.slideUp(500, function(){ $(this).remove() });\r\n          }\r\n          \r\n          $(\".cart-wrp .item-count\").text(response.count);\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initEmptyCart = function(){\r\n    $(\".summ .btn-empty\").on(\"click\", function(){\r\n      $.ajax({\r\n        url: \"/cart\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"emptyCart\",\r\n        },\r\n        success:function(response) {\r\n          emptyCart();\r\n          $(\".cart-wrp .item-count\").text(response.count);\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initOrder = function(){\r\n    $(\".summ .btn-order\").on(\"click\", function(){\r\n      $.ajax({\r\n        url: \"/cart\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"placeOrder\",\r\n        },\r\n        success:function(response) {\r\n          emptyCart();\r\n          $(\".cart-wrp .item-count\").text(response.count);\r\n          $(response.message).appendTo(\"body\").fadeIn(500).delay(2000).fadeOut(500, function(){ $(this).remove() });\r\n        }\r\n      });\r\n    });\r\n  }\r\n\r\n  initNumberSelector = function(){\r\n    var $numberSelector = $(\".number-selector\");\r\n    var step            = parseInt($numberSelector.attr(\"data-step\"));\r\n    var min             = parseInt($numberSelector.attr(\"data-min\"));\r\n    var max             = parseInt($numberSelector.attr(\"data-max\"));\r\n\r\n    var updateQuantity = function($button, quantity){\r\n      var price           = parseFloat($button.parents(\".meal\").attr(\"data-price\"));\r\n      var oldPrice        = parseFloat($button.parent().siblings(\".price\").find(\"strong\").text());\r\n      var oldGlobalPrice  = parseFloat($(\".summ .price strong\").text());\r\n\r\n      var newPrice        = quantity * price;\r\n      var newGlobalPrice  = oldGlobalPrice - oldPrice + newPrice;\r\n\r\n      $button.siblings(\".number\").text(quantity);\r\n      $button.parent().siblings(\".price\").find(\"strong\").text(newPrice.toFixed(2));\r\n      $(\".summ .price strong\").text(newGlobalPrice.toFixed(2));\r\n\r\n      $.ajax({\r\n        url: \"/cart\",\r\n        type: \"post\",\r\n        data: {\r\n          action: \"updateQuantity\",\r\n          id: $button.parents(\".meal\").attr(\"id\"),\r\n          quantity: $button.siblings(\".number\").text()\r\n        }\r\n      });\r\n    }\r\n\r\n    $(\".number-selector .plus\").on(\"click\", function(){\r\n      var val = parseInt($(this).siblings(\".number\").text());\r\n      if(val + step <= max) \r\n        updateQuantity($(this), val + step);\r\n    });\r\n\r\n    $(\".number-selector .minus\").on(\"click\", function(){\r\n      var val = parseInt($(this).siblings(\".number\").text());\r\n      if(val - step >= min) \r\n        updateQuantity($(this), val - step);\r\n    });\r\n  }\r\n\r\n})(jQuery);\n\n//# sourceURL=webpack://fristaurant/./public/assets/js/cart.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./public/assets/js/cart.js"]();
/******/ 	
/******/ })()
;