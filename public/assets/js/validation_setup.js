(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initValidation();
  });

  //Functions
  initValidation = function(){
    var emailVal = $("#email").val();

    jQuery.validator.addMethod("notEqualTo", function(value, element) {
      return value != emailVal;
    });

    jQuery.validator.addMethod("phone", function(value, element) {
      var pattern = new RegExp('^[+][0-9]{6,14}$');
      return pattern.test(value);
    });

    jQuery.extend(jQuery.validator.messages, {
      required: '<img src="assets/icons/exclamation.svg" alt=""> This field is required',
      email: '<img src="assets/icons/exclamation.svg" alt=""> Enter valid e-mail',
      minlength: '<img src="assets/icons/exclamation.svg" alt=""> Enter at least {0} characters',
      maxlength: '<img src="assets/icons/exclamation.svg" alt=""> Enter no more than {0} characters',
      equalTo: '<img src="assets/icons/exclamation.svg" alt=""> Passwords do not match',
      notEqualTo: '<img src="assets/icons/exclamation.svg" alt=""> This e-mail is already used',
      phone: '<img src="assets/icons/exclamation.svg" alt=""> Enter valid telephone number without spaces',
      number: '<img src="assets/icons/exclamation.svg" alt=""> Enter a number',
      min: '<img src="assets/icons/exclamation.svg" alt=""> Enter value greather than {0}'
    });
  };

})(jQuery);