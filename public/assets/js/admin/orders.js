(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initCheckOrders();

    initAcceptOrder();
    initRejectOrder();
    initDeliveredOrder();

    initNumberSelector();
  });

  //Functions
  initCheckOrders = function(){
    checkNewOrders = function(){
      $.ajax({
        url: "/orders",
        type: "post",
        data: {
          action: "check",
        },
        success:function(response) {
          if(response.more) {
            $(response.more).appendTo(".new").hide().slideDown(500);
          }
          
        }
      });
    }
    setInterval(checkNewOrders, 10000);
  }

  initRejectOrder = function(){
    $(".new").on("click", ".order .admin .btn-reject", function(){
      $order = $(this).parents(".order");

      $.ajax({
        url: "/orders",
        type: "post",
        data: {
          action: "reject",
          id: $order.attr("data-id")
        },
        success:function() {
          $order.slideUp(500, function(){ $(this).remove });
        }
      });
    });
  }

  initAcceptOrder = function(){
    $(".new").on("click", ".order .admin .btn-accept", function(){
      var $admin = $(this).parent();
      var $order = $(this).parents(".order");
      var time = $(this).siblings(".number-selector").children(".number").text();
      
      $.ajax({
        url: "/orders",
        type: "post",
        data: {
          action: "accept",
          id: $order.attr("data-id"),
          time: time
        },
        success:function(response) {
          $admin.delay(250).html('<p class="time">' + time + 'm</p><button class="btn-delivered">Delivered</button>');
          $order.slideUp(500, function(){ $order.remove() });
          $order.clone().appendTo(".accepted").hide().slideDown(500);
        }
      });
    });
  }

  initDeliveredOrder = function(){
    $(".accepted").on("click", ".order .admin .btn-delivered", function(){
      var $order = $(this).parents(".order");
      
      $.ajax({
        url: "/orders",
        type: "post",
        data: {
          action: "delivered",
          id: $order.attr("data-id")
        },
        success:function() {
          $order.slideUp(500, function(){ $order.remove() });
        }
      });
    });
  }

  initNumberSelector = function(){
    var $numberSelector = $(".number-selector");
    var step            = parseInt($numberSelector.attr("data-step"));
    var min             = parseInt($numberSelector.attr("data-min"));
    var max             = parseInt($numberSelector.attr("data-max"));

    $(".new").on("click", ".order .admin .number-selector .plus", function(){
      var val = parseInt($(this).siblings(".number").text());
      if(val + step <= max)
        $(this).siblings(".number").text(val + step);
    });

    $(".new").on("click", ".order .admin .number-selector .minus", function(){
      var val = parseInt($(this).siblings(".number").text());
      if(val - step >= min)
        $(this).siblings(".number").text(val - step);
    });
  }
  
})(jQuery);