(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initCreateCategory();
    initUpdateCategory();
    initDeleteCategory();
    initActiveCategory();

    initCreateMeal();
    initDeleteMeal();
  });

  //Other functions
  var checkCategoryLength = function(){
    if($(".categories .category").length == 0) {
      $(".meals .new-meal").slideUp(500);
      $(".no-category").slideDown(500);
    } else {
      $(".meals .new-meal").slideDown(500);
      $(".no-category").slideUp(500);
    }
  }

  //Functions
  initCreateCategory = function(){
    $(".new-category > .add").on("click", function(){
      $("#new-category").submit();
    });

    $("#new-category").validate({
      rules: {
        category: {
          required: true,
          minlength: 3,
          maxlength: 60
        }
      },
      submitHandler: function(form) {
        var serialized = $("#new-category").serializeArray();
        var category = serialized[0]["value"];

        $.ajax({
          url: "/admin",
          type: "post",
          data: {
            action: "createCategory",
            category: category
          },
          success:function(response) {
            $(response.category).insertBefore(".new-category").hide().slideDown(500);

            $('<div class="category-meals" id="category-meals-' + response.newID + '"></div>').insertBefore(".meals .new-meal");

            if($(".categories .category").length == 1) {
              $(".categories").find(".category").first().children(".name").click();
              checkCategoryLength();
            }
          }
        });
      }
    });
  }

  initUpdateCategory = function(){
    $(".categories").on("click", ".category .update", function(){
      $(this).siblings(".name").attr("readonly", false).addClass("focused").focus();
    });

    $(".categories").on("blur", ".category input.focused", function(){
      var $this     = $(this);
      var value     = $this.val();
      var prevValue = $this.attr("data-prev");

      $this.attr("readonly", true).removeClass("focused");

      if(value.length < 3) {
        $(".flash-message-v2.failure").text("Fill at least 3 characters").slideDown(500).delay(1250).slideUp(500);
        $this.val(prevValue);

      } else if(value.length > 60) {
        $(".flash-message-v2.failure").text("Fill less than 60 characters").slideDown(500).delay(1250).slideUp(500);
        $this.val(prevValue);

      } else if(value != prevValue){
        $.ajax({
          url:"/admin",
          type: "post",
          data:{
            action: "updateCategory",
            id: $this.parent().attr("data-id"),
            category: value
          },
          success:function() {
            $(".flash-message-v2.success").text("Category successfuly updated!").slideDown(500).delay(1250).slideUp(500);
            $this.attr("data-prev", value);
          }
        });
      }
    });
  };

  initDeleteCategory = function(){
    $(".categories").on("click", ".category .delete", function(){
      var $category = $(this).parent();
      var wasActive = $category.hasClass("active");
      
      $.ajax({
        url: "/admin",
        type: "post",
        data: {
          action: "deleteCategory",
          id: $category.attr("data-id")
        },
        success:function() {
          $(".meals #category-meals-" + $category.attr("data-id")).slideUp(500, function(){ $(this).remove() });
          $category.slideUp(500, function(){ 
            $category.remove();
            if(wasActive){
              $(".categories").find(".category").first().children(".name").click();
            }
            checkCategoryLength();
          });
        }
      });

      
    });
  }

  initActiveCategory = function(){
    $(".categories").on("click", ".category .name", function(){
      var id = $(this).parent().attr("data-id");

      $(".categories .active").removeClass("active");
      $(this).parent().addClass("active");
      $(".meals .active").removeClass("active");
      $(".meals #category-meals-" + id).addClass("active");
    });
  }





  initCreateMeal = function(){
    $("#new-meal").validate({
      rules: {
        meal: {
          required: true,
          minlength: 3,
          maxlength: 100
        },
        ingredients: {
          maxlength: 255
        },
        price: {
          required: true,
          number: true,
          min: 0
        }
      },
      submitHandler: function(form) {
        var serialized = $("#new-meal").serializeArray();
        var meal = serialized[0]["value"];
        var ingredients = serialized[serialized.length - 2]["value"];
        var price = serialized[serialized.length - 1]["value"];
        var allergens;
        if(serialized[1]["name"] != "ingredients") {
          allergens = "*(";
          for (let i = 1; i <= serialized.length - 3; i++) {
            allergens += serialized[i]["value"];
            if(i + 1 <= serialized.length - 3)
              allergens += ", ";
          }
          allergens += ")";
        }

        $.ajax({
          url: "/admin",
          type: "post",
          data: {
            action: "createMeal",
            meal: meal,
            allergens: allergens,
            ingredients: ingredients,
            price: price,
            category_id: $(".categories .category.active").attr("data-id")
          },
          success:function(response) {
            $(response.meal).appendTo(".meals .category-meals.active").hide().slideDown(500);
          }
        });
      }
    });
  }

  initDeleteMeal = function(){
    $(".meals").on("click", ".category-meals .meal .others .delete", function(){
      $meal = $(this).parents(".meal");

      $.ajax({
        url: "/admin",
        type: "post",
        data: {
          action: "deleteMeal",
          id: $meal.attr("data-id")
        },
        success:function() {
          $meal.slideUp(500);
        }
      });
    });
  }
  
})(jQuery);