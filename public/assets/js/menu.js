(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initAddToCart();
  });

  //Functions
  initAddToCart = function(){
    $(".price.button").on("click", function(){
      $.ajax({
        url: "/menu",
        type: "post",
        data: {
          id: $(this).parent().attr("data-id")
        },
        success:function(response) {
          if(response.message) {
            $(response.message).appendTo("body").fadeIn(500).delay(1500).fadeOut(500, function(){ $(this).remove() });
          } else {
            $(".cart-wrp .item-count").text(response.count);
          }
        }
      });
    });
  }

})(jQuery);