(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initBackButton();
  });

  //Functions
  initBackButton = function(){
    $(".back-btn").on("click", function(){
      window.history.back();
    });
  };
  
})(jQuery);