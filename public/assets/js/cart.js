(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initRemoveMeal();
    initEmptyCart();
    initOrder();

    initNumberSelector();
  });

  //Other functions
  emptyCart = function(){
    $(".filled").fadeOut(500, function(){ 
      $(".empty").fadeIn(500);
      $(this).remove();
    });
  }

  //Functions
  initRemoveMeal = function(){
    $(".meal .delete").on("click", function(){
      var $meal = $(this).parent();

      $.ajax({
        url: "/cart",
        type: "post",
        data: {
          action: "removeMeal",
          id: $meal.attr("id")
        },
        success:function(response) {
          var $meals = $(".meals .meal").length - 1;
          if($meals == 0) {
            emptyCart();
          } else {
            var price = parseFloat($meal.find(".price strong").text());
            var globalPrice = parseFloat($(".summ .price strong").text());
            $(".summ .price strong").text((globalPrice - price).toFixed(2));
            $meal.slideUp(500, function(){ $(this).remove() });
          }
          
          $(".cart-wrp .item-count").text(response.count);
        }
      });
    });
  }

  initEmptyCart = function(){
    $(".summ .btn-empty").on("click", function(){
      $.ajax({
        url: "/cart",
        type: "post",
        data: {
          action: "emptyCart",
        },
        success:function(response) {
          emptyCart();
          $(".cart-wrp .item-count").text(response.count);
        }
      });
    });
  }

  initOrder = function(){
    $(".summ .btn-order").on("click", function(){
      $.ajax({
        url: "/cart",
        type: "post",
        data: {
          action: "placeOrder",
        },
        success:function(response) {
          emptyCart();
          $(".cart-wrp .item-count").text(response.count);
          $(response.message).appendTo("body").fadeIn(500).delay(2000).fadeOut(500, function(){ $(this).remove() });
        }
      });
    });
  }

  initNumberSelector = function(){
    var $numberSelector = $(".number-selector");
    var step            = parseInt($numberSelector.attr("data-step"));
    var min             = parseInt($numberSelector.attr("data-min"));
    var max             = parseInt($numberSelector.attr("data-max"));

    var updateQuantity = function($button, quantity){
      var price           = parseFloat($button.parents(".meal").attr("data-price"));
      var oldPrice        = parseFloat($button.parent().siblings(".price").find("strong").text());
      var oldGlobalPrice  = parseFloat($(".summ .price strong").text());

      var newPrice        = quantity * price;
      var newGlobalPrice  = oldGlobalPrice - oldPrice + newPrice;

      $button.siblings(".number").text(quantity);
      $button.parent().siblings(".price").find("strong").text(newPrice.toFixed(2));
      $(".summ .price strong").text(newGlobalPrice.toFixed(2));

      $.ajax({
        url: "/cart",
        type: "post",
        data: {
          action: "updateQuantity",
          id: $button.parents(".meal").attr("id"),
          quantity: $button.siblings(".number").text()
        }
      });
    }

    $(".number-selector .plus").on("click", function(){
      var val = parseInt($(this).siblings(".number").text());
      if(val + step <= max) 
        updateQuantity($(this), val + step);
    });

    $(".number-selector .minus").on("click", function(){
      var val = parseInt($(this).siblings(".number").text());
      if(val - step >= min) 
        updateQuantity($(this), val - step);
    });
  }

})(jQuery);