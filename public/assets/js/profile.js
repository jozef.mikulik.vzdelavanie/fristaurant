(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initSections();
    initUpdateForm();
    initChangeForm();
  });

  //Functions
  initSections = function(){
    $("button").on("click", function(){
      var section = $(this).attr("data-section");

      $(this).parent().siblings(".active").removeClass("active");
      $(this).parent().addClass("active");

      $(".section.active").removeClass("active");
      $("#"+section).addClass("active");
    });
  };

  initUpdateForm = function(){
    $("#update-form").validate({
      rules: {
        name: {
          required: true,
          minlength: 3,
          maxlength: 60
        },
        address: {
          maxlength: 60
        },
        city: {
          maxlength: 60
        },
        telephone: {
          required: true,
          maxlength: 15,
          phone: true
        },
        email: {
          required: true,
          email: true,
          maxlength: 60
        }
      }
    });
  };

  initChangeForm = function(){
    $("#change-form").validate({
      rules: {
        password: {
          required: true,
          minlength: 8,
          maxlength: 255
        },
        password_conf: {
          required: true,
          equalTo: '#password'
        }
      }
    });
  }

})(jQuery);