(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initShowOrder();
  });

  //Functions
  initShowOrder = function(){
    $(".order .main").on("click", function(){
      $(this).siblings(".meals").slideToggle(500);
    });
  }

})(jQuery);