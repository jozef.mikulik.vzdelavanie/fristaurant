(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initDropdownNav();
    initSignedInDropdown();
  });

  //Functions
  initSignedInDropdown = function(){
    $(".signedin").on({
      mouseenter: function(){
        $(this).children(".drp-menu-wrp").stop().slideDown(250);
      },
      mouseleave: function(){
        $(this).children(".drp-menu-wrp").stop().slideUp(250);
      }
    });
  };

  initDropdownNav = function(){
    var $icon = $("header .navigation-drp-icon");
    var $iTop = $("header .navigation-drp-icon .top");
    var $iMid = $("header .navigation-drp-icon .mid");
    var $iBot = $("header .navigation-drp-icon .bot");
    var $dropdown = $("header .navigation-drp");

    var navHidden = true;
    var duration = 200;
    var top = $iTop.css("top");
    var mid = $iMid.css("top");
    var bot = $iBot.css("top");

    var changeToX = function(){
      navHidden = false;
      
      $iTop.animate({top: mid}, duration).animate({rotate: "45deg"}, duration);
      $iMid.delay(duration).animate({opacity: 0}, 0);
      $iBot.animate({top: mid}, duration).animate({rotate: "-45deg"}, duration);

      $dropdown.slideDown(duration * 2);
    };

    var changeToBurger = function() {
      navHidden = true;

      $iTop.animate({rotate: "0deg"}, duration).animate({top: top}, duration);
      $iMid.delay(duration).animate({opacity: 1}, 0);
      $iBot.animate({rotate: "0deg"}, duration).animate({top: bot}, duration);
      
      $dropdown.slideUp(duration * 2);
    }

    $icon.on("click", function(){
      navHidden ? changeToX() : changeToBurger();
    });
    
    $(window).on("resize", function(){
      if(!window.matchMedia('(max-width: 1023px)').matches)
        !navHidden ? changeToBurger() : "";
    });
    
  }

  
})(jQuery);