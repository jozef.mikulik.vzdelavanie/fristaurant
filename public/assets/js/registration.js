(function ($) {

  //Initialize functions
  $(document).ready(function () {
    initFormValidation();
  });

  //Functions
  initFormValidation = function(){
    $("form").validate({
      rules: {
        name: {
          required: true,
          minlength: 3,
          maxlength: 60
        },
        address: {
          maxlength: 60
        },
        city: {
          maxlength: 60
        },
        telephone: {
          required: true,
          maxlength: 15,
          phone: true
        },
        email: {
          required: true,
          email: true,
          maxlength: 60,
          notEqualTo: true
        },
        password: {
          required: true,
          minlength: 8,
          maxlength: 255
        },
        password_conf: {
          required: true,
          equalTo: '#password'
        }
      }
    });
  }

})(jQuery);