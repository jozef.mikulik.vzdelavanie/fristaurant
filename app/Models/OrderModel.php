<?php namespace App\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
  protected $table      = "tb_orders";
  protected $primaryKey = "id";
  protected $allowedFields = ["order", "user_id", "price", "state", "date", "time"];
}