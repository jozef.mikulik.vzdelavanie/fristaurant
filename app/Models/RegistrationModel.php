<?php namespace App\Models;

use CodeIgniter\Model;

class RegistrationModel extends Model
{
  protected $table      = "tb_users";
  protected $primaryKey = "id";
  protected $allowedFields = ["name", "address", "city", "telephone", "email", "password", "salt"];

  protected $beforeInsert = ["passwordHash"];
  protected $beforeUpdate = ["passwordHash", "updateSession"];

  protected function passwordHash(array $data) {
    if(isset($data["data"]["password"])) {
      $data["data"]["salt"] = substr(md5(rand()), -5);
      $data["data"]["password"] .= $data["data"]["salt"];
      $data["data"]["password"] = password_hash($data["data"]["password"], PASSWORD_DEFAULT);
    }
    
    return $data;
  }

  protected function updateSession(array $data){
    if(isset($data["data"]["email"]) && $data["data"]["email"] != session()->get("email"))
      session()->set(["email" => $data["data"]["email"]]);

    if(isset($data["data"]["name"]) && $data["data"]["name"] != session()->get("name"))
      session()->set(["name" => $data["data"]["name"]]);

    return $data;
  }
}