<?php namespace App\Models\Admin;

use CodeIgniter\Database\ConnectionInterface;

class MenuModel {
  protected $db;

  public function __construct(ConnectionInterface &$db) {
    $this->db =& $db;
  }

  function getCategories() {
    return $this->db->table('tb_meal_categories')->get()->getResultArray();
  }

  function getMeals($category_id) {
    return $this->db->table("tb_meals")//->get()->getResultArray();
              ->where("category_id", $category_id)->get()->getResultArray();
  }

  function createCategory($data) {
    $this->db->table("tb_meal_categories")->insert($data);
    return $this->db->insertID();
  }

  function updateCategory($data) {
    $this->db->table("tb_meal_categories")->set("category", $data["category"])->where("id", $data["id"])->update();
  }

  function deleteCategory($data) {
    $this->db->table("tb_meal_categories")->delete($data);
  }

  function createMeal($data) {
    $this->db->table("tb_meals")->insert($data);
    return $this->db->insertID();
  }

  function deleteMeal($data) {
    $this->db->table("tb_meals")->set("category_id", NULL)->where("id", $data["id"])->update();
  }

  function getMeal($id) {
    return $this->db->table("tb_meals")->where("id", $id)->get()->getResultArray();
  }

}