<?php namespace App\Controllers;

use App\Models\Admin\MenuModel;

class Home extends BaseController
{
  public function index() {
    $data["sideblocks"] = [];
    array_push($data["sideblocks"], ["heading" => "High quality meat", "img-src" => "assets/images/steak.jpg", "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur egestas fermentum sem vel pulvinar. Cras ut nulla dolor. Phasellus ut condimentum enim. Ut urna felis, rutrum sit amet euismod sed, rutrum in neque. Nullam dapibus vitae metus nec pretium. Nullam blandit gravida mi. Aenean gravida eget ante gravida auctor. Proin et nunc in justo mattis molestie. Donec dapibus dictum blandit. Vestibulum a elementum enim. Etiam nec nulla convallis, vulputate diam eu, porttitor elit. Donec laoreet erat eu orci blandit hendrerit. "]); 
    array_push($data["sideblocks"], ["heading" => "Vegetables from local farmers", "img-src" => "assets/images/spinach.jpg", "content" => "Nunc vulputate tortor a arcu dictum porttitor. Maecenas rutrum tincidunt ex, ac iaculis urna fringilla at. In tempus porttitor orci, a lobortis lacus finibus in. Sed quis mi sed leo ultricies feugiat. Pellentesque accumsan elementum urna, quis rutrum est laoreet sit amet. Phasellus tortor lectus, euismod et enim eu, mollis tincidunt orci. Mauris id magna purus. Nam felis velit, fringilla ut convallis a, dictum ac orci. Nulla ut urna in purus elementum tempus. "]); 
    array_push($data["sideblocks"], ["heading" => "Exclusive, selected coffee", "img-src" => "assets/images/coffee.jpg", "content" => "Nulla ac massa vitae ante ullamcorper accumsan. Vestibulum iaculis hendrerit tellus eget pulvinar. Vivamus pulvinar tellus laoreet nibh eleifend gravida. Morbi rhoncus erat diam, eget suscipit leo pretium ac. Sed a lacus a dolor cursus convallis. Phasellus elementum facilisis sapien eu ornare. Morbi volutpat ligula id quam vulputate sagittis porttitor vel tortor. Fusce aliquam tincidunt lorem, in molestie massa gravida non. Vestibulum tincidunt neque arcu, et pellentesque tellus consectetur hendrerit. Morbi eleifend eget tortor ac posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus augue diam, faucibus id neque ac, elementum convallis turpis. Phasellus convallis felis nec nisl sollicitudin tempor. Suspendisse mollis finibus sollicitudin. Duis quis pulvinar nulla, consequat malesuada mi. Morbi dapibus rhoncus metus eu rhoncus. "]);
    array_push($data["sideblocks"], ["heading" => "Sweet desserts", "img-src" => "assets/images/pancakes.jpg", "content" => "Pellentesque et ullamcorper libero. Cras tempus ac dui nec sodales. Duis non fermentum odio. Suspendisse potenti. Donec varius purus commodo lorem molestie, ac efficitur dolor ornare. Aliquam erat volutpat. Fusce cursus arcu a ipsum consequat, eu blandit quam hendrerit. Nam sollicitudin felis eu nisl sagittis scelerisque. Duis dictum volutpat tempor. Sed fermentum nibh eget nisi vulputate vehicula. Aliquam nec ex fermentum, venenatis risus ut, tincidunt felis. Praesent ultrices, purus ut sollicitudin ullamcorper, nunc tortor placerat turpis, eget fermentum arcu elit at nisi. Mauris id faucibus tellus, in rutrum felis. "]);

    return view('home', $data);
  }

  public function menu() {
    $db = db_connect();
    $model = new MenuModel($db);

    $data["categories"] = [];
    $categories = $model->getCategories();
    foreach($categories as $category) {
      $data["categories"][$category["category"]] = $model->getMeals($category["id"]);
    }

    return view("menu", $data);
  }

  public function menuAddToCart() {
    $cart = session()->get("cart");
    $id = "id".strval($this->request->getPost("id"));

    if(count($cart) >= 15) {
      $data["message"] = view("components/flash_message", ["message" => "Already max. number of products in cart!", "type" => "failure"]);
    } else {
      foreach($cart as $product_id => $quantity) {
        if($id == $product_id)
          $data["message"] = view("components/flash_message", ["message" => "Product is already in the cart!", "type" => "failure"]);
      }
    }

    if(isset($data["message"])) {
      return $this->response->setJSON($data);
    } else {
      session()->push("cart", [ $id => 1]);
      $data["count"] = count(session()->get("cart"));
      return $this->response->setJSON($data);
    }
  }

  public function contact() {
    return view("contact");
  }
}
