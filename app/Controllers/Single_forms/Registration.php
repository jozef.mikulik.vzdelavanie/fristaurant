<?php namespace App\Controllers\Single_forms;

use App\Controllers\BaseController;
use App\Models\RegistrationModel;

class Registration extends BaseController
{
  public function index() {
    return $this->render();
  }

  public function register() {
    if($this->request->getMethod() == "post") {
      $rules = [
        "name"          => "required|min_length[3]|max_length[60]",
        "address"       => "max_length[60]",
        "city"          => "max_length[60]",
        "telephone"     => "required|max_length[60]",
        "email"          => [
          "rules"         => "required|max_length[60]|is_unique[tb_users.email]",
          "errors"        => [
            "is_unique"    => "This e-mail is already used"
          ]
        ],
        "password"      => "required|min_length[8]|max_length[255]",
        "password_conf" => "matches[password]",
      ];

      if(!$this->validate($rules)) {  
        $data["validation"] = $this->validator;
        return $this->render($data);
      
      } else {
        $model = new RegistrationModel();
        $model->save([
          "name"      => $this->request->getVar("name"),
          "address"   => $this->request->getVar("address"),
          "city"      => $this->request->getVar("city"),
          "telephone" => $this->request->getVar("telephone"),
          "email"     => $this->request->getVar("email"),
          "password"  => $this->request->getVar("password"),
        ]);

        session()->setFlashdata("success", "Your account has been successfully created!");
        return redirect()->to("/signin");
      }
    }
  }

  private function render($data = []) {
    $data = array_merge(
      [
        "heading"     => "registration",
        "action"      => "/registration",
        "js"          => "registration",
        "input_grps"  => [
          "1" => [
            ["text", "name", "Name"],
            ["text", "address", "Address"],
            ["text", "city", "City/Village"],
            ["text", "telephone", "Telephone (+421xxxxxxxxx)"]
          ],
          "2" => [
            ["text", "email", "E-mail"],
            ["password", "password", "Password"],
            ["password", "password_conf", "Confirm password"],
            ["submit", "register"]
          ]
        ]
      ],
      $data
    );

    return view("single_form", $data); 
  }
}