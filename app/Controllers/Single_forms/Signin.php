<?php namespace App\Controllers\Single_forms;

use App\Controllers\BaseController;
use App\Models\RegistrationModel;

class Signin extends BaseController
{
  public function index() {
    return $this->render();
  }

  public function signin() {
    if($this->request->getMethod() == "post") {
      $rules = [ "password"  => "signedUser[email, password]" ];

      if(!$this->validate($rules)) {  
        $data["failure"] = "Incorrect E-mail or Password!";
        return $this->render($data); 

      } else {
        $model = new RegistrationModel();
        $user = $model->where("email", $this->request->getVar("email"))->first(); 

        switch($user["type"]) {
          case  "u":
            $data = [
              "id"    => $user["id"],
              "name"  => $user["name"],
              "email" => $user["email"],
              "type"  => "user",
              "cart"  => []
            ];
            session()->set($data);
            return redirect()->to("/menu"); 

          default:
            $data = [
              "id"        => $user["id"],
              "name"      => "Admin",
              "type"      => "admin",
              "newOrders" => 0
            ];
            session()->set($data);
            return redirect()->to("/admin");
        }
      }
    }
  }

  private function render($data = []) {
    $data = array_merge(
      [
        "heading"     => "signin",
        "action"      => "/signin",
        "input_grps"  => [
          "1" => [
            ["text", "email", "E-mail"],
            ["password", "password", "Password"],
            ["submit", "signin"]
          ]
        ],
        "links"       => [
          ["/registration", "Create account"]
        ] 
      ],
      $data
    );

    return view("single_form", $data);
  }
}