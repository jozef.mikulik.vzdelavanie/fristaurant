<?php namespace App\Controllers;

use App\Models\Admin\MenuModel;
use App\Models\OrderModel;
use App\Models\RegistrationModel;

class Admin extends BaseController
{
  //MENU
  public function menu() {
    $db = db_connect();
    $model = new MenuModel($db);

    $data['categories'] = $model->getCategories();
    $data["meals"] = [];
    foreach($data['categories'] as $category) {
      $data["meals"][$category["id"]] = $model->getMeals($category["id"]);
    }

    $data["allergens"] = [
      1   => "Gluten",  2   => "Crustaceans",   3   => "Eggs",
      4   => "Fish",    5   => "Peanuts",       6   => "Soya",
      7   => "Milk",    8   => "Nuts",          9   => "Celery",
      10  => "Mustard", 11  => "Sesame",        12  => "Sulphites",
      13  => "Lupin",   14  => "Moluscs"
    ];

    $data['page'] = "menu";
    return view('admin/menu', $data);
  }

  public function menuAjax() {
    $db = db_connect();
    $model = new MenuModel($db);

    switch($this->request->getPost("action")) {
      case "createCategory": 
        $data["newID"] = $model->createCategory(["category" => $this->request->getPost("category")]);
        $data["category"] = view("components/admin/menu/single_category", ["id" => $data["newID"], "category" => $this->request->getPost("category")]);
        break;

      case "updateCategory":
        $model->updateCategory([
          "id" => $this->request->getPost("id"),
          "category" => $this->request->getPost("category")
        ]);
        break;
      
      case "deleteCategory":
        $model->deleteCategory(["id" => $this->request->getPost("id")]);
        break;

      case "createMeal":
        $meal = [
          "meal"        => $this->request->getPost("meal"),
          "ingredients" => $this->request->getPost("ingredients"),
          "allergens"   => $this->request->getPost("allergens"),
          "price"       => $this->request->getPost("price"),
          "category_id" => $this->request->getPost("category_id"),
        ];
        $id = $model->createMeal($meal);
        $meal["id"] = $id;
        $data["meal"] = view("components/admin/menu/single_meal", $meal);
        break;

      case "deleteMeal":
        $model->deleteMeal(["id" => $this->request->getPost("id")]);
        break;
    }
        
    if(isset($data)) {
      return $this->response->setJSON($data);
    }
  }




  //ORDERS
  public function orders() {
    $orderModel = new OrderModel();

    $data["newOrders"] = $this->getOrders($orderModel->where("state", 0)->findAll());
    $data["acceptedOrders"] = $this->getOrders($orderModel->where("state", 1)->findAll());
    
    session()->set("newOrders", count($data["newOrders"]));

    $data["page"] = "orders";
    return view('admin/orders', $data);
  }

  public function ordersAjax(){
    $orderModel = new OrderModel();

    switch($this->request->getPost("action")) {
      case "check":
        if(session()->get("newOrders") < count($orderModel->where("state", 0)->findAll())) {
          $firstNew = array_values($this->getOrders($orderModel->where("state", 0)->findAll(1, session()->get("newOrders"))))[0];
          $data["more"] = view("components/admin/orders/single_order", $firstNew);
          session()->set("newOrders", session()->get("newOrders") + 1);
        }
        break;

      case "reject":
        $orderModel->update($this->request->getPost("id"), ["state" => -1]);
        session()->set("newOrders", session()->get("newOrders") - 1);
        break;

      case "accept":
        $orderModel->update($this->request->getPost("id"), ["state" => 1, "time" => $this->request->getPost("time")]);
        session()->set("newOrders", session()->get("newOrders") - 1);
        break;

      case "delivered":
        $orderModel->update($this->request->getPost("id"), ["state" => 2]);
        break;
          
    }

    if(isset($data)) {
      return $this->response->setJSON($data);
    }
  }




  //HELPER
  private function getOrders($rawOrders) {
    $db = db_connect();
    $menuModel = new MenuModel($db);
    $userModel = new RegistrationModel();

    $orders = [];
    foreach($rawOrders as $rawOrder) {
      $orders[$rawOrder["id"]]["state"] = $rawOrder["state"];
      $orders[$rawOrder["id"]]["time"]  = $rawOrder["time"];
      $orders[$rawOrder["id"]]["id"]    = $rawOrder["id"];
      $orders[$rawOrder["id"]]["user"]  = $userModel->find($rawOrder["user_id"]);
      $orders[$rawOrder["id"]]["price"] = $rawOrder["price"];
      $orders[$rawOrder["id"]]["meals"] = [];

      $items = explode(" ", $rawOrder["order"]);
      unset($items[count($items) - 1]);
      foreach($items as $item) {
        $explode = explode("x", $item);
        $orders[$rawOrder["id"]]["meals"][$explode[0]] = ($menuModel->getMeal($explode[0]))[0];
        $orders[$rawOrder["id"]]["meals"][$explode[0]]["quantity"] = $explode[1];
      }
      $orders[$rawOrder["id"]]["datetime"] = $rawOrder["date"];
    }

    return $orders;
  }

}
