<?php namespace App\Controllers;

use App\Models\RegistrationModel;
use App\Models\Admin\MenuModel;
use App\Models\OrderModel;

class User extends BaseController
{
  public function index() {
    return $this->render();
  }

  public function updateUser(){
    if($this->request->getMethod() == "post") {
      
      if($this->request->getPost("password") != null){
        $rules = [ "password_old"  => "correctPassword[password_old]" ];

        if(!$this->validate($rules)) {
          $data["failure"] = "Wrong current password!";
          
        } else {
          $data["success"] = "Password has been changed successfully!";
          $newData = [
            "id" => session()->get("id"),
            "password" => $this->request->getPost("password")
          ];
          $model = new RegistrationModel();
          $model->save($newData);
        }

        $data["active"] = "change";
        return $this->render($data);

      } else {
        $rules = [ "email" => "is_unique[tb_users.email]" ];

        $newData = [
          "id"        => session()->get("id"),
          "name"      => $this->request->getPost("name"),
          "address"   => $this->request->getPost("address"),
          "city"      => $this->request->getPost("city"),
          "telephone" => $this->request->getPost("telephone")
        ];

        if(!$this->validate($rules) && $this->request->getPost("email") != session()->get("email")) {
          $data["failure"] = "Email was not chaned - is used by different user!";
          
        } else {
          $data["success"] = "All changed fields have been successfully changed!";
          $newData["email"] = $this->request->getPost("email");
        }

        $model = new RegistrationModel();
        $model->save($newData);

        $data["active"] = "update";
        return $this->render($data);
      }
    }
  }

  public function deleteUser() {
    $model = new RegistrationModel();
    $user = $model->find(session()->get("id"));

    if($user) {
      $model->delete(session()->get("id"));
      return $this->logout();

    } else {
      return redirect()->to("/");
    }
  }

  public function logout() {
    session()->destroy();
    return redirect()->to("/");
  }

  public function cart() {
    $cart = session()->get("cart");
    
    $db = db_connect();
    $model = new MenuModel($db);

    $meals = [];
    $i = 0;
    foreach($cart as $strID => $quantity) {
      $id = intval(substr($strID, 2));
      array_push($meals, ($model->getMeal($id))[0]);
      $meals[$i]["quantity"] = $quantity;
      $i++;
    }
    $data["meals"] = $meals;

    return view("cart", $data);
  }

  public function cartAjax() {
    switch($this->request->getPost("action")) {
      case "removeMeal":
        unset($_SESSION["cart"][$this->request->getPost("id")]);
        break;

      case "emptyCart":
        session()->set("cart", []);
        break;
        
      case "placeOrder":
        $cart = session()->get("cart");
        $order = "";
        $price = 0;
        $db = db_connect();
        $menu = new MenuModel($db);
        foreach($cart as $strID => $quantity) {
          $id = intval(substr($strID, 2));
          $price += ($menu->getMeal($id))[0]["price"] * $quantity;
          $order .= strval($id)."x".strval($quantity)." ";
        }
        $model = new OrderModel();
        $model->save([
          "order" => $order,
          "user_id" => session()->get("id"),
          "price" => $price,
          "state" => 0
        ]);
        session()->set("cart", []);
        $data["message"] = view("components/flash_message", ["message" => "Your order has been sent", "type" =>  "success"]);
        break;

      case "updateQuantity":
        $_SESSION["cart"][$this->request->getPost("id")] = $this->request->getPost("quantity");
        break;
    }

    $data["count"] = count(session()->get("cart"));
    return $this->response->setJSON($data);
  }

  public function myOrders() {
    $db = db_connect();
    $menuModel = new MenuModel($db);
    $orderModel = new OrderModel();
    $userModel = new RegistrationModel();

    $orders = [];
    foreach($orderModel->where("user_id", session()->get("id"))->findAll() as $rawOrder) {
      $orders[$rawOrder["id"]]["state"] = $rawOrder["state"];
      $orders[$rawOrder["id"]]["time"] = $rawOrder["time"];
      $orders[$rawOrder["id"]]["price"] = $rawOrder["price"];
      $orders[$rawOrder["id"]]["meals"] = [];

      $items = explode(" ", $rawOrder["order"]);
      unset($items[count($items) - 1]);
      foreach($items as $item) {
        $explode = explode("x", $item);
        $orders[$rawOrder["id"]]["meals"][$explode[0]] = ($menuModel->getMeal($explode[0]))[0];
        $orders[$rawOrder["id"]]["meals"][$explode[0]]["quantity"] = $explode[1];
      }
      $orders[$rawOrder["id"]]["datetime"] = $rawOrder["date"];
    }

    $data["orders"] = $orders;
    return view("my_orders", $data);
  }


  
  private function render($data = ["active" => "update"]) {
    $model = new RegistrationModel();
    $user = $model->where("id", session()->get("id"))->first();
    $data["user"] = $user;
    return view("profile", $data);
  }
}
