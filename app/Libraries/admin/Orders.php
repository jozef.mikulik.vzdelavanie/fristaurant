<?php namespace App\Libraries\Admin;

class Orders {
  public function getOrder($order) {
    return view("components/admin/orders/single_order", $order);
  }
}