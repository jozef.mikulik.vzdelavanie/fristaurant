<?php namespace App\Libraries\Admin;

class Menu {
  public function getCategory($category) {
    return view("components/admin/menu/single_category", $category);
  }

  public function getMeal($meal) {
    return view("components/admin/menu/single_meal", $meal);
  }
}