<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class Permission implements FilterInterface
{
  public function before(RequestInterface $request, $arguments = null)
  {
    $access = false;
    foreach($arguments as $argument) {
      if( ($argument == "not" && !session()->get("type")) || $argument == session()->get("type") ) {
        $access = true;
        break;
      }
    }
    if(!$access)
      return redirect()->back();
  }

  public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
  {
      // Do something here
  }
}