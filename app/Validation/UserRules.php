<?php namespace App\Validation;

use App\Models\RegistrationModel;

class UserRules {
  public function signedUser(string $str, string $fields, array $data) {
    $model = new RegistrationModel();
    $user = $model->where("email", $data["email"])->first();

    if(!$user)
      return false;

    return password_verify($data["password"].$user["salt"], $user["password"]);
  }

  public function correctPassword(string $str, string $fields, array $data) {
    $model = new RegistrationModel();
    $user = $model->where("id", session()->get("id"))->first();
    return password_verify($data["password_old"].$user["salt"], $user["password"]);
  }
}