<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/contact.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?> 
  <script src="build/js/header.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<?= $this->include("page-parts/header") ?>

<div class="container content">
  <div class="row">

    <div class="col-lg-12">
      <h1 class="main-heading">Contact us</h1>
    </div>

    <div class="col-lg-12">
      <h2>Contact information</h2>
      <div class="informations">
        <div class="information">
          <img src="assets/icons/phone-primary.svg" alt="phone icon">
          <a href="tel:+421901234567">+421 901 234 567</a>
        </div>
        <div class="information">
          <img src="assets/icons/envelope-primary.svg" alt="envelope icon">
          <a href="mailto:info@fristaurant.sk">info@fristaurant.sk</a>
        </div>
        <div class="information">
          <img src="assets/icons/marker-primary.svg" alt="map marker icon">
          <p>Univerzitná 8215/1, 010 26 Žilina</p>
        </div>
      </div>
      <h2>Opening hours</h2>
      <div class="opening">
        <div class="days">
          <p>Monday</p>
          <p>Monday</p>
          <p>Wednesday</p>
          <p>Thursday</p>
          <p>Friday</p>
          <p>Saturday</p>
          <p>Sunday</p>
        </div>
        <div class="hours">
          <p>10:00 - 21:00</p>
          <p>10:00 - 21:00</p>
          <p>10:00 - 21:00</p>
          <p>10:00 - 21:00</p>
          <p>10:00 - 23:00</p>
          <p>10:00 - 23:00</p>
          <p>10:00 - 21:00</p>
        </div>
      </div>
      <h2>Socials</h2>
      <div class="socials">
        <a href="https://www.facebook.com/" target="_blank"><img src="assets/icons/facebook.svg" alt="facebook logo"></a>
        <a href="https://www.twitter.com/" target="_blank"><img src="assets/icons/twitter.svg" alt="twitter logo"></a>
      </div>
    </div>

  </div>
</div>

<?= $this->endSection() ?>