<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/cart.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/header.js"></script>
  <script src="build/js/cart.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<?= $this->include("page-parts/header") ?>

<div class="container cart">
  <div class="empty" style="<?= count($meals) != 0 ? "display: none;" : "" ?>">
    <p>Your cart is empty</p>
    <a href="/menu">Menu</a>
  </div>

  <div class="filled" style="<?= count($meals) == 0 ? "display: none;" : "" ?>">
    <div class="main-heading">
      <h1>Your Cart</h1>
    </div>

    <div class="meals">
      <?php $price = 0 ?>
      <?php foreach($meals as $meal) : ?>
        <?php $price += $meal["price"] * $meal["quantity"] ?>
        <div class="meal" id="id<?= $meal["id"] ?>" data-price="<?= $meal["price"] ?>">
          <h3><?= $meal["meal"] ?></h3>
          <p class="price"><strong><?= number_format($meal["price"] * $meal["quantity"], 2)?></strong> €</p>
          <div class="number-selector" data-min="1" data-max="99" data-step="1">
            <p class="minus">-</p>
            <p class="number"><?= $meal["quantity"] ?></p>
            <p class="plus">+</p>
          </div>
          <img src="assets/icons/delete-error.svg" alt="delete-icon" class="delete">
        </div>
      <?php endforeach; ?>
    </div>

    <div class="summ">
      <button class="btn-empty">Empty</button>
      <p class="price"><strong><?= number_format($price, 2)?></strong> €</p>
      <button class="btn-order">Order</button>
    </div>
  </div>
</div>

<?= $this->endSection() ?>