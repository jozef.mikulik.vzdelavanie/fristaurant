<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/menu.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/header.js"></script>
  <script src="build/js/menu.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<?= $this->include("page-parts/header") ?>

<div class="container">
   <div class="main-heading">
      <h1>Menu</h1>
   </div>
</div>

<div class="container">
  <div class="menu">
    <div class="row">

      <div class="col-lg-12">
        <?php foreach($categories as $name => $meals) : ?>
          <?php if(count($meals) > 0) : ?>
            <div class="category">
              <h3><?= $name; ?></h3>
              <?php foreach($meals as $meal) : ?>
                <div class="meal" data-id="<?= $meal["id"] ?>">
                  <div class="content">
                    <p class="name"><?= $meal["meal"]; ?></p>
                    <p class="desc"><?= $meal["ingredients"]; ?> <?= $meal["allergens"] ?></p>
                  </div>
                  <div class="price <?= session()->get("id") ? "button" : "" ?>">
                    <?= number_format($meal["price"], 2) ?> €
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>

    </div>
  </div>
</div>

<?= $this->include("page-parts/footer") ?>

<?= $this->endSection() ?>