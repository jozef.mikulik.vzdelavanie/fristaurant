<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/my_orders.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/header.js"></script>
  <script src="build/js/my_orders.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<?= $this->include("page-parts/header") ?>

<div class="container orders">
  <div class="main-heading">
    <h1>My Orders</h1>
  </div>

  <?php foreach($orders as $order) : ?>
    <div class="order <?= $order["state"] == 0 ? "sent" : ($order["state"] == 1 ? "accepted" : ($order["state"] == 2 ? "delivered" : "rejected")) ?>">
      <div class="main">
        <div class="state">
          <?php 
          switch($order["state"]) {
            case 0: echo "S"; break;
            case 1: echo $order["time"]."m"; break;
            case 2: echo "D"; break;
            case -1: echo "R";
          } 
          ?>
        </div>
        <p class="date"><?= date_format(date_create($order["datetime"]), "d.m.Y - H:i") ?></p>
        <p class="price"><?= number_format($order["price"], 2) ?>  €</p>
      </div>
      <div class="meals">
        <?php foreach($order["meals"] as $meal) : ?>
          <p><strong><?= $meal["quantity"] ?>x</strong> <?= $meal["meal"] ?></p>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endforeach; ?>
</div>

<?= $this->endSection() ?>