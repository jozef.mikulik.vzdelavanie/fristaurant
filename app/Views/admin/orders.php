<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/admin/orders.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/admin/orders.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>
<?= $this->include("page-parts/admin_sidenav") ?>

<div class="new">
  <h2>New orders</h2> 
  <?php foreach($newOrders as $order) : ?>
    <?= view_cell("\App\Libraries\Admin\Orders::getOrder", $order); ?>
  <?php endforeach; ?>
</div>

<div class="accepted">
  <h2>Accepted orders</h2>
  <?php foreach($acceptedOrders as $order) : ?>
    <?= view_cell("\App\Libraries\Admin\Orders::getOrder", $order); ?>
  <?php endforeach; ?>
</div>



<?= $this->endSection() ?>