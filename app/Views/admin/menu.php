<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/admin/menu.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/admin/menu.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>
<?= $this->include("page-parts/admin_sidenav") ?>

<div class="menu">
  <div class="categories">
    <h1>Categories</h1>

    <?php foreach($categories as $category) : ?>
      <?php $category["class"] = $categories[0]["id"] == $category["id"] ? "active" : ""; ?>
      <?= view_cell("\App\Libraries\Admin\Menu::getCategory", $category); ?>
    <?php endforeach; ?>

    <div class="no-category" style="<?= count($categories) != 0 ? "display:none" : ""?>">
      There is no category
    </div>

    <form class="new-category" id="new-category">
      <div class="input-wrp">
        <input type="text" name="category" placeholder="New Category">
      </div>
      <img src="assets/icons/plus-square-primary.svg" alt="" class="add">
    </form>

  </div>
  <div class="meals">
    <h2>Meals</h2>
    <?php foreach($meals as $id => $categor_meals) : ?>
      <div class="category-meals <?= array_keys($meals)[0] == $id ? "active" : "" ?>" id="category-meals-<?= $id ?>">
      <?php foreach($categor_meals as $meal) : ?>
        <?= view_cell("\App\Libraries\Admin\Menu::getMeal", $meal); ?>
      <?php endforeach; ?>
      </div>
    <?php endforeach; ?>

    <div class="no-category" style="<?= count($categories) != 0 ? "display:none" : ""?>">
      No category selected
    </div>

    <div class="new-meal" style="<?= count($categories) == 0 ? "display:none" : ""?>">
      <form action="/admin/orders" method="post" id="new-meal">
        <div class="row">
          
          <div class="col-lg-5 col-md-12">
            <div class="input-wrp">
              <input type="text" name="meal" placeholder="Meal name">
            </div>
            <div class="allergens">
              <h3>Allergens</h3>
              <div class="row">
                <?php foreach($allergens as $code => $allergen) : ?>
                  <div class="col-lg-4">
                    <input type="checkbox" value="<?= $code ?>" name="<?= $allergen ?>"> 
                    <label><?= $allergen ?></label>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
          
          <div class="col-lg-7 col-md-12">
            <div class="input-wrp">
              <textarea name="ingredients" placeholder="Ingredients"></textarea>
            </div>
            
            <div class="row">
              <div class="col-lg-4">
                <div class="input-wrp">
                 <input type="text" name="price">
                </div>
                
              </div>
              <div class="col-lg-8">
                <div class="input-wrp"></div>
                <input type="submit" value="Add meal">
              </div>
            </div>
          </div>

        </div>
      </form>
    </div>

  </div>
</div>

<div class="flash-message-v2 failure"></div>
<div class="flash-message-v2 success"></div>

<?= $this->endSection() ?>