<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/home.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/header.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<div class="intro">
   <img src="assets/images/logo.png" alt="logo of fristaurant">
   <a href="#header" class="continue">
      <p>Continue</p>
      <img src="assets/icons/arrow-down.svg" alt="arrow down icon">
   </a>
</div>

<?= $this->include("page-parts/header") ?>

<div class="container heading">
   <h1>Something about us...</h1>
</div>

<?php foreach($sideblocks as $index => $side_block) : ?>
  <div class="container lg side-block">
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 content <?= $index % 2 == 0 ? "" : "right" ?>">
            <h2><?= $side_block["heading"]; ?></h2>
            <p><?= $side_block["content"] ?></p>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 img-wrp <?= $index % 2 == 0 ? "right" : "left" ?>">
        <img src="<?= $side_block["img-src"]; ?>" class="img-cover" alt="illustrative photo of food">
      </div>
    </div>
  </div>
<?php endforeach; ?>

<?= $this->include("page-parts/footer") ?>

<?= $this->endSection() ?>