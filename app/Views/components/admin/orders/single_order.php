<div class="order row" data-id="<?= $id ?>">
  <div class="meals col-lg-7">
    <?php foreach($meals as $meal) : ?>
      <p><strong><?= $meal["quantity"] ?>x</strong><?= $meal["meal"] ?></p>
    <?php endforeach; ?>
  </div>

  <div class="user col-lg-5">
    <p><strong>Name:</strong><?= $user["name"] ?></p>
    <p><strong>Address:</strong><?= $user["address"] ?>, <?= $user["city"] ?></p>
    <p><strong>Tel:</strong><?= $user["telephone"] ?></p>
    <p><strong>Time:</strong><?= date_format(date_create($datetime), "H:i") ?></p>
    <p><strong>Price:</strong><?= number_format($price, 2) ?> €</p>
  </div>

  <div class="col-lg-12">
    <div class="admin">
      <?php if($state == 0) : ?>
        <button class="btn-reject">Reject</button>
        <div class="number-selector" data-min="15" data-max="300" data-step="15">
          <p class="minus">-</p>
          <p class="number">45</p>
          <p class="plus">+</p>
        </div>
        <button class="btn-accept">Accept</button>
      <?php else : ?>
        <p class="time"><?= $time ?>m</p>
        <button class="btn-delivered">Delivered</button>
      <?php endif; ?>
      
    </div>
  </div>
</div>