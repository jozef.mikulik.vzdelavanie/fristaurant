<div class="category <?= isset($class) ? $class : "" ?>" data-id="<?= $id ?>">
  <input type="text" class="name" value="<?= $category ?>" readonly data-prev="<?= $category ?>">
  <img src="assets/icons/pencil-primary.svg" alt="" class="update">
  <img src="assets/icons/delete-error.svg" alt="" class="delete">
</div> 