<div class="meal" data-id="<?= $id ?>">
  <div class="content">
    <p class="name"><?= $meal ?></p>
    <p class="ingredients"><?= $ingredients ?> <?= $allergens ?></p>
  </div>
  <div class="others">
    <p class="price"><?= number_format($price, 2)  ?> €</p>
    <img src="assets/icons/delete-error.svg" alt="" class="delete">
  </div>
</div>