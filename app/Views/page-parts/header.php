<header id="header">

  <a href="/home" class="logo-wrp">
    <img src="assets/images/logo.png" class="logo" alt="logo of fristaurant">
  </a>   
  
  <div class="navigation">
    <ul>
      <li><a href="/menu">Menu</a></li>
      <li><a href="/contact">Contact</a></li>
    </ul>
  </div>
  
  <?php if(session()->get("id")) : ?>
    <div class="cart-wrp">
      <a href="/cart">
        <img src="assets/icons/cart-primary.svg" alt="">
        <div class="item-count"><?= count(session()->get("cart")) ?></div>
      </a>
    </div>
  <?php endif; ?>

  <div class="signin-wrp">
    <?php if(session()->get("id")) : ?>
      <div class="signedin">
        <p><?= session()->get("name") ?></p> 
        <img src="assets/icons/arrow-down-primary.svg" alt="">
        <div class="drp-menu-wrp">
          <div class="drp-menu">
            <a href="/myorders">My orders</a>
            <a href="/profile">Profile</a>
            <a href="/logout">Logout</a>
          </div>
        </div>
      </div>
    <?php else : ?>
      <a class="signin" href="/signin">
        <p>Sign in</p>
        <img src="assets/icons/sign-in-primary.svg" alt="sign in icon">
      </a>
    <?php endif; ?>
  </div>

  <div class="navigation-drp-icon">
    <div class="line top"></div>
    <div class="line mid"></div>
    <div class="line bot"></div>
  </div>
   
  <div class="navigation-drp">
    <div class="links">
      <a href="/menu" class="link">Menu</a>
      <a href="/contact" class="link">Contact</a>
      <?php if(session()->get("id")) : ?>
        <a href="/myorders" class="link user">My orders</a>
        <a href="/profile" class="link user">Profile</a>
        <a href="/logout" class="link user">logout</a>
      <?php else : ?>
        <a href="/signin" class="link">Sign in</a>
      <?php endif; ?>
    </div>
  </div>
   
</header>