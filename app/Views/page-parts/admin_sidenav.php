<div class="admin-sidenav">
  <a href="/admin">
    <img src="assets/icons/admin/<?= $page == "menu" ? "menu-secondary.svg" : "menu-primary.svg" ?>" alt="" class="<?= $page == "menu" ? "active" : "" ?>">
  </a>
  <a href="/orders">
    <img src="assets/icons/admin/<?= $page == "orders" ? "orders-secondary.svg" : "orders-primary.svg" ?>" alt="" class="<?= $page == "orders" ? "active" : "" ?>">
  </a> 
  <a href="/logout" class="logout">
    <img src="/assets/icons/admin/power-off-primary.svg" alt="">
  </a>
</div>