<footer>
  <div class="container">
    <div class="footer">
      <div class="socials">
        <a href="https://www.facebook.com/" target="_blank">
          <img src="assets/icons/facebook.svg" alt="facebook logo">
        </a>
        <a href="https://www.twitter.com/" target="_blank">
          <img src="assets/icons/twitter.svg" alt="twitter logo">
        </a>
      </div>
    </div>
  </div>
</footer>