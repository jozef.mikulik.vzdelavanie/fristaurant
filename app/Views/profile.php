<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/profile.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?> 
  <script src="build/js/header.js"></script>
  <script src="build/js/profile.js"></script>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<?= $this->include("page-parts/header") ?>

<div class="container sm content">
  <div class="row">
    
    <div class="col-lg-12">
      <h1>User profile</h1>
      <?php if(isset($success)) : ?> 
        <div class="flash-message success">
          <?= $success ?>
        </div>
      <?php endif; ?>
      <?php if(isset($failure)) : ?> 
        <div class="flash-message failure">
          <img src="assets/icons/exclamation.svg" alt=""><?= $failure ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="col-lg-12">
      <div class="sections">
        
        <div class="section <?= $active == "update" ? "active" : "" ?>" id="update">
          <form action="/profile" method="post" id="update-form" autocomplete="off">
            
            <div class="row">
              <div class="col-lg-6 col-sm-12">
                <div class="input-wrp">
                  <label for="name" class="normal">Name</label>
                  <input type="text" name="name" id="name" value="<?= $user["name"] ?>">
                </div>
                <div class="input-wrp">
                  <label for="city" class="normal">City/Village</label>
                  <input type="text" name="city" id="city" value="<?= $user["city"] ?>">
                </div>
                <div class="input-wrp">
                  
                  <?php if (isset($validation) && $validation->hasError("email")) : ?>
                    <label for="email" class="normal">E-mail</label>
                    <input type="text" name="email" id="email" value="<?= isset($_POST["email"]) ? $_POST["email"] : "" ?>" class="error">
                    <label id="email-error" class="error" for="email"><img src="assets/icons/exclamation.svg" alt=""><?= $validation->getError("email") ?></label>
                  <?php else : ?>
                    <label for="email" class="normal">E-mail</label>
                    <input type="text" name="email" id="email" value="<?= $user["email"] ?>">
                  <?php endif; ?>
                </div>
              </div>
              <div class="col-lg-6 col-sm-12">
                <div class="input-wrp">
                  <label for="address" class="normal">Address</label>
                  <input type="text" name="address" id="address" value="<?= $user["address"] ?>">
                </div>
                <div class="input-wrp">
                  <label for="telephone" class="normal">Telephone</label>
                  <input type="text" name="telephone" id="telephone" value="<?= $user["telephone"] ?>">
                </div>
                <input type="submit" value="update" class="inactive">
              </div>
            </div>

          </form>
        </div>

        <div class="section <?= $active == "change" ? "active" : "" ?>" id="change">
          <form action="/profile" method="post" id="change-form" autocomplete="off">
            <div class="row">
              
              <div class="col-lg-6 col-sm-12">
                <div class="input-wrp">
                  <label for="password" class="normal">New password</label>
                  <input type="password" name="password" id="password">
                </div>
                <div class="input-wrp">
                  <label for="password_conf" class="normal">Confirm new password</label>
                  <input type="password" name="password_conf" id="password_conf">
                </div>
              </div>
              <div class="col-lg-6 col-sm-12">
                <div class="input-wrp">
                  <label for="password_old" class="normal">Current password</label>
                  <input type="password" name="password_old" id="password_old">
                </div>
                <input type="submit" value="change">
              </div>

            </div>
          </form>
        </div>

      </div>
    </div>

    <div class="col-lg-12">
      <div class="buttons">
        <div class="row">
          
          <div class="col-lg-6 button <?= $active == "update" ? "active" : "" ?>">
            <button class="active" data-section="update">Update profile</button>
          </div>
          <div class="col-lg-6 button <?= $active == "change" ? "active" : "" ?>">
            <button data-section="change">Change password</button>
          </div>
          <div class="col-lg-6 button">
            <a href="/delete">Delete account</a>
          </div>
          
        </div>
      </div>
    </div>

  </div>
</div>

<?= $this->endSection() ?>