<?= $this->extend("layouts/primary") ?>

<?= $this->section("styles") ?>
  <link rel="stylesheet" href="build/css/single_form.css">
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
  <script src="build/js/single_form.js"></script>
  <?php if(isset($js)) : ?>
    <script src="build/js/<?= $js ?>.js"></script>
  <?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section("content") ?>

<img src="assets/images/logo-vertical.png" alt="" class="logo">

<a href="/signin" class="back-btn">
  <img src="assets/icons/arrow-left-background.svg" alt="">
</a>

<div class="container">
  <h1><?= $heading ?></h1>
  
  <?php if(session()->get("success")) : ?> 
    <div class="flash-message success">
      <?= session()->get("success") ?>
    </div>
  <?php endif; ?>
  <?php if(isset($failure)) : ?> 
    <div class="flash-message failure">
      <img src="assets/icons/exclamation.svg" alt=""><?= $failure ?>
    </div>
  <?php endif; ?>


  <form action="<?= $action ?>" method="post" autocomplete="off">
    <div class="row">

      <?php foreach($input_grps as $input_grp) : ?>
        <div class="<?= count($input_grps) == 1 ? "single-col" : "col-lg-".(12 / count($input_grps)) ?> col-sm-12">
          <?php foreach($input_grp as $input) : ?>

            <?php if($input[0] == "submit") : ?>
              <input type="submit" value="<?= $input[1] ?>">
              <?php break; ?>
            <?php endif; ?>
            
            <div class="input-wrp">
              <?php if (isset($validation) && $validation->hasError($input[1])) : ?>
                <input type="<?= $input[0] ?>" name="<?= $input[1] ?>" id="<?= $input[1] ?>" placeholder="<?= $input[2] ?>" value="<?= isset($_POST[$input[1]]) ? $_POST[$input[1]] : "" ?>" class="error">
                <label id="<?= $input[1] ?>-error" class="error" for="<?= $input[1] ?>"><img src="assets/icons/exclamation.svg" alt=""><?= $validation->getError($input[1]) ?></label>
              <?php else : ?>
                <input type="<?= $input[0] ?>" name="<?= $input[1] ?>" id="<?= $input[1] ?>" placeholder="<?= $input[2] ?>" value="<?= isset($_POST[$input[1]]) ? $_POST[$input[1]] : "" ?>">
              <?php endif; ?>
            </div>

          <?php endforeach; ?>     
        </div>
      <?php endforeach; ?>

    </div>
  </form>
  <?php if(isset($links)) : ?>
    <div class="links">
      <?php foreach($links as $link) : ?>
        <a href="<?= $link[0] ?>"><?= $link[1] ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>

<?= $this->endSection() ?>