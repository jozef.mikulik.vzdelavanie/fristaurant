<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FRIstaurant</title>
    <link rel="shortcut icon" href="assets/icons/logo.ico">
    <link rel="stylesheet" href="build/css/style.css">
    <!--<script src="/node_modules/jquery/dist/jquery.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
    <script src="build/js/validation_setup.js"></script>
    <?= $this->renderSection("styles"); ?>
    <?= $this->renderSection("scripts"); ?>
</head>
<body>

  <?= $this->renderSection("content"); ?>

</body>
</html>