const path = require("path");

const scripts = {
  "js/header": path.resolve(__dirname, "public/assets/js/header.js"),
  "js/menu": path.resolve(__dirname, "public/assets/js/menu.js"),
  "js/single_form": path.resolve(__dirname, "public/assets/js/single_form.js"),
  "js/registration": path.resolve(__dirname, "public/assets/js/registration.js"),
  "js/profile": path.resolve(__dirname, "public/assets/js/profile.js"),
  "js/validation_setup": path.resolve(__dirname, "public/assets/js/validation_setup.js"),
  "js/admin/menu": path.resolve(__dirname, "public/assets/js/admin/menu.js"),
  "js/admin/orders": path.resolve(__dirname, "public/assets/js/admin/orders.js"),
  "js/cart": path.resolve(__dirname, "public/assets/js/cart.js"),
  "js/my_orders": path.resolve(__dirname, "public/assets/js/my_orders.js"),
};

const styles = {
  "css/style": path.resolve(__dirname, "public/assets/css/style.scss"),
  "css/home": path.resolve(__dirname, "public/assets/css/home.scss"),
  "css/menu": path.resolve(__dirname, "public/assets/css/menu.scss"),
  "css/contact": path.resolve(__dirname, "public/assets/css/contact.scss"),
  "css/single_form": path.resolve(__dirname, "public/assets/css/single_form.scss"),
  "css/profile": path.resolve(__dirname, "public/assets/css/profile.scss"),
  "css/admin/menu": path.resolve(__dirname, "public/assets/css/admin/menu.scss"),
  "css/admin/orders": path.resolve(__dirname, "public/assets/css/admin/orders.scss"),
  "css/cart": path.resolve(__dirname, "public/assets/css/cart.scss"),
  "css/my_orders": path.resolve(__dirname, "public/assets/css/my_orders.scss"),
};

module.exports = {
  ...scripts,
  ...styles,
};
